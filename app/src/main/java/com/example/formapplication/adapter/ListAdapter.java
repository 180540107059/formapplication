package com.example.formapplication.adapter;

import android.content.Context;
import android.provider.SyncStateContract;
import android.telecom.ConnectionService;
import android.text.LoginFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.formapplication.R;
import com.example.formapplication.util.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

public class ListAdapter extends BaseAdapter {

    Context context ;
    ArrayList<HashMap<String , Object>> userList;

    public ListAdapter(Context context ,ArrayList<HashMap<String , Object>> userList){
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.list_users , null);
        TextView tvName = view1.findViewById(R.id.tvLstName);
        TextView tvPhone = view1.findViewById(R.id.tvLstPhone);
        TextView tvEmail = view1.findViewById(R.id.tvLstEmail);
        TextView tvGender = view1.findViewById(R.id.tvLstGender);

        tvName.setText(userList.get(position).get("UserName").toString());
        Log.d("name::" , "" +userList.get(position).get("UserName").toString());
        tvPhone.setText(userList.get(position).get("Contact").toString());
        tvEmail.setText(userList.get(position).get("Email").toString());

        Log.d("gender adapter" , "" + userList.get(position).get("Gender"));

        if(userList.get(position).get("Gender").toString().equals("1")){
            tvGender.setText("M");
            tvGender.setBackground(context.getResources().getDrawable(R.drawable.male_bg));
        }else if(userList.get(position).get("Gender").toString().equals("0")){
            tvGender.setText("F");
            tvGender.setBackground(context.getResources().getDrawable(R.drawable.female_bg));
        }

        return view1;
    }
}
