package com.example.formapplication.util;

public class Constants {
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String PHONE_NUMBER = "phone";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String HOBBY = "hobby";

}
