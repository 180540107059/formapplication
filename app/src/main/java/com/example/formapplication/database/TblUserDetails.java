package com.example.formapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.formapplication.util.Constants;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class TblUserDetails extends UserListDB {

    public static final String TABLE_NAME = "Tbl_UserDetails";
    public static final String USER_ID = "UserID";
    public static final String USER_NAME = "UserName";
    public static final String CONTACT = "Contact";
    public static final String EMAIL = "Email";
    public static final String GENDER = "Gender";
    public static final String HOBBIES = "Hobbies";

    public ArrayList<HashMap<String , Object>> userList = new ArrayList<>();


    public TblUserDetails(Context context) {
        super(context);
    }

    public long insertUserDetail(String name , String num , String email , int gender , String hobbies){

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME , name);
        cv.put(CONTACT , num);
        cv.put(EMAIL , email);
        cv.put(GENDER , gender);
        cv.put(HOBBIES , hobbies);
        long lastInsertedID = db.insert(TABLE_NAME , null , cv);
        db.close();
        return lastInsertedID;
    }

    public ArrayList<HashMap<String, Object>> displayUserInserted(){

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query ,null);
        cursor.moveToFirst();

        if(cursor != null){
            while(!cursor.isAfterLast()){

                HashMap<String , Object> map = new HashMap<>();
                map.put(USER_NAME , cursor.getString(1));
                map.put(CONTACT , cursor.getString(2));
                map.put(EMAIL , cursor.getString(3));
                map.put(GENDER , cursor.getString(4));
                map.put(HOBBIES , cursor.getString(5));
                userList.add(map);
                cursor.moveToNext();
            }
            cursor.close();
        }
        db.close();
        return userList;

    }
}
