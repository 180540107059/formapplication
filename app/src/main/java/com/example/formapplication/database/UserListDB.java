package com.example.formapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class UserListDB extends SQLiteAssetHelper {

    public static final String DATABASE_NAME = "UserListDB.db";
    public static final int DATABASE_VERSION = 1;

    public UserListDB(Context context) {
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
    }
}
