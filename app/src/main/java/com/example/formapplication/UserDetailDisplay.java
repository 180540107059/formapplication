package com.example.formapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.formapplication.util.Constants;

import java.util.HashMap;

public class UserDetailDisplay extends AppCompatActivity {

    TextView tvName , tvEmail , tvPhone , tvGender , tvHobbies;
    HashMap<String , Object> map = new HashMap<>();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details_display);
        InitViewReference();
        bindViewValues();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void bindViewValues() {
        Intent intent3 = getIntent();
        map = (HashMap<String, Object>) intent3.getSerializableExtra("userList");

        Log.d("hash map" , "" + map);

        tvName.setText(map.get("UserName").toString());
        tvPhone.setText(map.get("Contact").toString());
        tvEmail.setText(map.get("Email").toString());
        tvHobbies.setText(map.get("Hobbies").toString());

        if(map.get("Gender").toString().equals("1")){
            tvGender.setText("M");
            tvGender.setBackground(getDrawable(R.drawable.male_bg));
        }else if(map.get("Gender").toString().equals("0")){
            tvGender.setText("F");
            tvGender.setBackground(getDrawable(R.drawable.female_bg));
        }

    }

    private void InitViewReference() {
        tvName = findViewById(R.id.tvActName);
        tvEmail = findViewById(R.id.tvActEmail);
        tvPhone = findViewById(R.id.tvActPhoneNumber);
        tvGender = findViewById(R.id.tvActDisplayGender);
        tvHobbies = findViewById(R.id.tvActDisplayHobby);
    }

}
