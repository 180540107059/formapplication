package com.example.formapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.formapplication.adapter.ListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class DisplayActivity extends AppCompatActivity {

    ListView lvusers;
    ListAdapter listAdp;
    ArrayList<HashMap<String , Object>> userList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        initViewRef();
        bindViewValues();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void bindViewValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));

        Log.d("Details :: " , "" + userList);

        listAdp = new ListAdapter(this , userList);
        lvusers.setAdapter(listAdp);

        lvusers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("selected user" , "" + userList.get(i));
                Intent intent1 = new Intent(DisplayActivity.this , UserDetailDisplay.class);
                intent1.putExtra("userList" , userList.get(i));
                startActivity(intent1);
            }
        });
    }

    private void initViewRef() {
        lvusers = findViewById(R.id.lvDisplay);
    }
}