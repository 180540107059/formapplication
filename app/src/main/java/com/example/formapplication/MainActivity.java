package com.example.formapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.formapplication.database.TblUserDetails;
import com.example.formapplication.database.UserListDB;
import com.example.formapplication.util.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etEmail, etPhone;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    CheckBox cbFootball, cbCricket, cbSwimming, cbReading, cbSinging;
    Button add , list;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    ImageView ivClearFirstName , ivClearLastName , ivClearPhone , ivClearEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        ChbValidation();
        clrInput();
        initViewEvent();
    }



    private void clrInput() {

        ivClearFirstName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstName.getText().clear();
            }
        });
        ivClearLastName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etLastName.getText().clear();
            }
        });
        ivClearPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhone.getText().clear();
            }
        });
        ivClearEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEmail.getText().clear();
            }
        });
    }

    private void ChbValidation() {

        if(R.id.rbActFemale == rgGender.getCheckedRadioButtonId()){
            cbCricket.setVisibility(View.GONE);
            cbFootball.setVisibility(View.GONE);
            cbSinging.setVisibility(View.VISIBLE);
            cbSwimming.setVisibility(View.VISIBLE);
            cbReading.setVisibility(View.VISIBLE);
        }

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if(rbMale.isChecked()){
                    cbCricket.setVisibility(View.VISIBLE);
                    cbFootball.setVisibility(View.VISIBLE);
                    cbSinging.setVisibility(View.VISIBLE);
                    cbSwimming.setVisibility(View.VISIBLE);
                    cbReading.setVisibility(View.VISIBLE);
                }

                else if(rbFemale.isChecked()){
                    cbCricket.setVisibility(View.GONE);
                    cbFootball.setVisibility(View.GONE);
                    cbSinging.setVisibility(View.VISIBLE);
                    cbSwimming.setVisibility(View.VISIBLE);
                    cbReading.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void initViewEvent() {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {

                    TblUserDetails tblUserDetails = new TblUserDetails(getApplicationContext());

                    String name = etFirstName.getText() + " " + etLastName.getText();
                    String num = etPhone.getText().toString();
                    String email = etEmail.getText().toString();
                    int gender = rbMale.isChecked() ? 1 : 0 ;

                    String hobbies = "";
                    if (cbCricket.isChecked()) {
                        hobbies += "," + cbCricket.getText().toString();
                    }
                    if (cbFootball.isChecked()) {
                        hobbies += "," + cbFootball.getText().toString();
                    }
                    if (cbSwimming.isChecked()) {
                        hobbies += "," + cbSwimming.getText().toString();
                    }
                    if (cbSinging.isChecked()) {
                        hobbies += "," + cbSinging.getText().toString();
                    }
                    if (cbReading.isChecked()) {
                        hobbies += "," + cbReading.getText().toString();
                    }
                    if (hobbies.length() > 0) {
                        hobbies = hobbies.substring(1);
                    }

                    long isInserted = tblUserDetails.insertUserDetail(name , num , email , gender , hobbies);

                    Toast.makeText(
                            getApplicationContext() ,
                            isInserted > 0 ? "User Inserted Successfully" : "Something went wrong" ,
                            Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                    intent.putExtra("UserList", tblUserDetails.displayUserInserted());
                    startActivity(intent);

                    etFirstName.getText().clear();
                    etLastName.getText().clear();
                    etPhone.getText().clear();
                    etEmail.getText().clear();
                    rbMale.setChecked(false);
                    rbFemale.setChecked(false);
                    cbCricket.setChecked(false);
                    cbFootball.setChecked(false);
                    cbReading.setChecked(false);
                    cbSinging.setChecked(false);
                    cbSwimming.setChecked(false);
                }
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TblUserDetails listUsers = new TblUserDetails(getApplicationContext());
                Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                intent.putExtra("UserList", listUsers.displayUserInserted());
                startActivity(intent);
            }
        });
    }

    private void initViewReference() {

        new UserListDB(getApplicationContext()).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etPhone = findViewById(R.id.etActPhone);
        etPhone.setText("9825978783"); // just so i dont have to enter everytime
        etEmail = findViewById(R.id.etActEmail);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        cbCricket = findViewById(R.id.cbActCricket);
        cbFootball = findViewById(R.id.cbActFootball);
        cbReading = findViewById(R.id.cbActReading);
        cbSinging = findViewById(R.id.cbActSinging);
        cbSwimming = findViewById(R.id.cbActSwimming);

        add = findViewById(R.id.btnActAdd);
        list = findViewById(R.id.btnActSeeList);

        ivClearFirstName = findViewById(R.id.ivClearFname);
        ivClearLastName = findViewById(R.id.ivClearLname);
        ivClearPhone = findViewById(R.id.ivClearPno);
        ivClearEmail = findViewById(R.id.ivClearEmail);
    }

    boolean isValid(){

        boolean flag = true;
        if (TextUtils.isEmpty(etFirstName.getText().toString())) {
            etFirstName.setError("Please enter first name");
            etFirstName.setCursorVisible(true);
            flag = false;
        } else if (etFirstName.getText().toString().contains(" ")) {
            etFirstName.setError("Please enter first name correctly");
            etFirstName.setCursorVisible(true);
            flag = false;
        } else {

        }

        if (TextUtils.isEmpty(etLastName.getText().toString())) {
            etLastName.setError("Please enter last name");
            etLastName.setCursorVisible(true);
            flag = false;
        } else if (etLastName.getText().toString().contains(" ")) {
            etLastName.setError("Please enter last name correctly");
            etLastName.setCursorVisible(true);
            flag = false;
        } else {

        }

        if(TextUtils.isEmpty(etEmail.getText().toString())){
            etEmail.setError("Please enter email address");
            etEmail.setCursorVisible(true);
            flag = false;
        }else{
            String email = etEmail.getText().toString();
            String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

            if(!(email.matches(emailPattern))){
                etEmail.setError("Please enter valid email address");
                etEmail.setCursorVisible(true);
                flag = false;
            }
        }

        if(TextUtils.isEmpty(etPhone.getText().toString())){
            etPhone.setError("Please enter phone number");
            etPhone.setCursorVisible(true);
            flag = false;
        }else{
            String phoneNumber = etPhone.getText().toString();
            if(phoneNumber.length() < 10 || phoneNumber.contains(" ")){
                etPhone.setError("Please enter a valid phone number");
                etPhone.setCursorVisible(true);
                flag = false;
            }
        }

        if(! (rbMale.isChecked()) && !(rbFemale.isChecked())){
            Toast.makeText(this , "Please enter your gender" , Toast.LENGTH_LONG).show();
            flag = false;
        }
        return flag;
    }

}